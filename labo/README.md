
# Laboratoires:

- Laboratoire 2 du 2019-09-16: Voir l'énoncé du Laboratoire 2 Dans le manuel d'exercices
- Laboratoire 1 du 2019-09-09: Voir l'énoncé du Laboratoire 1R1 Dans le manuel d'exercices 
- Laboratoire 3 du 2019-09-23: Voir l'énoncé du Laboratoire 5R01 Dans le manuel d'exercices 
- Laboratoire 4 du 2019-09-30: Voir l'énoncé du Laboratoire 7R01 Dans le manuel d'exercices
- Laboratoire noté 1 du 2019-10-07() et laboratoire 5 (Voir énoncé du laboratoire 9 dans le manuel d'exercices):  
- Laboratoire 5 du 2019-MM-JJ: Voir l'énoncé du Laboratoire 5 Dans le manuel d'exercice
- Laboratoire 6 du 2019-10-21: Voir l'énoncé du Laboratoire 10R01 Dans le manuel d'exercices
- Laboratoire 7 du 2019-10-28: Voir l'énoncé des  exercices bootstrap des Laboratoires 6 et 7 (exercices bootstrap seulement)  Dans le manuel d'exercice
- Laboratoire 8 du 2019-11-04: Voir l'énoncé du Laboratoire 10R02 Dans le manuel d'exercices
- Laboratoire noté 2 du 2019-11-11 () et Laboratoire 9 : Voir l'énoncé du Laboratoire 10R03 Dans le manuel d'exercice
- Laboratoire 10 du 2019-11-18: Voir l'énoncé du Laboratoire 16 (PHP) Dans le manuel d'exercice
- Laboratoire 11 du 2019-11-25: Voir l'énoncé du Laboratoire 17 (PHP -Formulaires) Dans le manuel d'exercice
- Laboratoire 12 du 2019-12-02: Voir l'énoncé du Laboratoire 18 dans le manuel d'exercice
- Laboratoire noté 3  du 2019-12-09et Laboratoire 13 : Voir les énoncés des Laboratoires 12 et 13 dans le manuel d'exercice
- Laboratoire 14 du 2019-12-10: à faire à la maison. Voir l'énoncé du Laboratoire 19 dans le manuel d'exercice

